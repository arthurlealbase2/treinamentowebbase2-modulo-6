package treinamentorestassured.Pratica;

import org.testng.annotations.Test;
import org.json.simple.JSONObject;

import static  io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;


public class CadastrarNovoPetSucesso {
    public Object Json(){
        JSONObject pet = new JSONObject();

        pet.put("id", 0);
        pet.put("petId", 0);
        pet.put("quantity", 0);
        pet.put("shipDate", "2022-02-11T09:37:36.650+0000");
        pet.put("status", "placed");
        pet.put("complete", true);

        return pet;
    }

    @Test
    public void CadastrarNovoPedidoDePetComSucesso(){
        given().
                baseUri("https://petstore.swagger.io/v2").
                header("content-type","application/json").
                body(Json()).
        when().
                post("/store/order").
        then().
                statusCode(200).
                and().
                body("id", equalTo( 9223372036854775807L),
                            "petId", equalTo(0),
                        "quantity", equalTo(0),
                        "shipDate", equalTo("2022-02-11T09:37:36.650+0000"),
                        "status", equalTo("placed"),
                        "complete", equalTo(true)
                        );

    }

}
