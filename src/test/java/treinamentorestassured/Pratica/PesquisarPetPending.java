package treinamentorestassured.Pratica;

import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class PesquisarPetPending {
    static String status = "pending";

    @Test
    public void PesquisarPorUmPetInexistente(){

        given().
                baseUri("https://petstore.swagger.io/v2").
                queryParam("status", status).
                when().
                get("/pet/findByStatus").
                then().
                body("status[0]", equalTo("pending"));
        //response.body("status", equalTo("pending"));

    }
}
