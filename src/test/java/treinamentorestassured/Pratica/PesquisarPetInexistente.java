package treinamentorestassured.Pratica;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class PesquisarPetInexistente {

    static String petId = "-2";

    @Test
    public void PesquisarPorUmPetInexistente(){
        given().
                baseUri("https://petstore.swagger.io/v2").
                pathParam("petId", petId).
        when().
                get("/pet/{petId}").

        then().
                statusCode(404).
                and().
                body("message",equalTo("Pet not found"));



    }
}
