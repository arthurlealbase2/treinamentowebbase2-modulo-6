package treinamentorestassured.Pratica;

import org.testng.annotations.Test;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import static  io.restassured.RestAssured.*;


public class AtualizarDadosPetExistente {

    public Object Json(){
        JSONObject pet = new JSONObject();
        JSONObject category = new JSONObject();
        JSONObject tag1 =  new JSONObject();
        JSONArray tags = new JSONArray();
        JSONArray photoURLs = new JSONArray();

        pet.put("id", 2);
        pet.put("name", "Shepherd");
        pet.put("status", "avaliable");

        category.put("id",2);
        category.put("name", "dog");
        pet.put("category", category);

        tag1.put("id",2);
        tag1.put("name","Sem raça definida");
        tags.add(tag1);
        pet.put("tags", tags);

        photoURLs.add("https://http.cat/100");
        photoURLs.add("https://http.cat/102");

        pet.put("photoURLs", photoURLs);

        return pet;
    }

    @Test
    public void AtualizarDadosDeUmPetExistenteComSucesso(){

        given().
                baseUri("https://petstore.swagger.io/v2").
                header("content-type","application/json").
                body(Json()).
        when().
                put("/pet").
        then().statusCode(200);
    }

}
