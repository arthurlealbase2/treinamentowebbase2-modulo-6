package treinamentorestassured.Pratica;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class RequisicaoMetodoInvalido {

    @Test
    public void RealizarRequisicaoInformandoMetodoInvalido(){
        given().
                baseUri("https://petstore.swagger.io/v2").
        when().
                get("/pet").
        then().statusCode(405);
    }
}
